package ru.swayfarer.simplecontentupdater.io;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.event.AbstractEvent;
import ru.swayfarer.swl3.updaterv3.content.FileEntry;
import ru.swayfarer.swl3.updaterv3.refresher.RefreshEvent;

@Data @EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@SuperBuilder
public class FileDownloadingEvent extends AbstractEvent {
    
    @NonNull
    public RefreshEvent refreshEvent;
    
    @NonNull
    public FileEntry fileEntry;
    
    @NonNull
    public IObservable<Long> eventDownloadProgress;
}
