package ru.swayfarer.simplecontentupdater.io;

import java.util.concurrent.atomic.AtomicLong;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.annotation.BeanName;
import ru.swayfarer.iocv3.annotation.InjectionByCfg;
import ru.swayfarer.iocv3.annotation.RequiredBean;
import ru.swayfarer.iocv3.annotation.helpers.Singleton;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.updaterv3.content.FileEntry;
import ru.swayfarer.swl3.updaterv3.io.FilesRefreshFuns;
import ru.swayfarer.swl3.updaterv3.io.IFileEntryUploader;
import ru.swayfarer.swl3.updaterv3.io.RefreshFuns;
import ru.swayfarer.swl3.updaterv3.refresher.RefreshEvent;

@BeanName("fileEntryUploader")
@Singleton
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class RetryFileDownloader implements IFileEntryUploader {

    public IObservable<FileDownloadingEvent> eventUploadingStart = Observables.createObservable();

    public ILogger logger = LogFactory.getLogger();
    
    @RequiredBean(required = false)
    @InjectionByCfg("retry.delay")
    public Integer retryDelay;
    
    @RequiredBean(required = false)
    @InjectionByCfg("retry.count")
    public Integer retryCount;
    
    public AtomicLong totalDownloads = new AtomicLong();
    
    @Override
    public String upload(FileEntry fileEntry, IFunction0<DataInStream> streamSource, RefreshEvent refreshEvent) throws Throwable
    {
        IObservable<Long> eventDownloadProgress = Observables.createObservable();
        
        var fileUploader = getUploader(eventDownloadProgress::next);
        
        var uploadingEvent = FileDownloadingEvent.builder()
                .fileEntry(fileEntry)
                .refreshEvent(refreshEvent)
                .eventDownloadProgress(eventDownloadProgress)
                .build()
        ;
        
        totalDownloads.incrementAndGet();
        logger.info("Starting downloading file entry", fileEntry.getPath(), ". Total downloads count " + totalDownloads);
        eventUploadingStart.next(uploadingEvent);
                
        var ret = fileUploader.apply(fileEntry, streamSource, refreshEvent);
        totalDownloads.decrementAndGet();
        logger.info("Completed downloading file entry", fileEntry.getPath(), ". Total downloads count " + totalDownloads);
        
        return ret;
    }
    
    public IFileEntryUploader getUploader(IFunction1NoR<Long> tracker)
    {
        var defaultUploader = FilesRefreshFuns.upload(tracker);
        
        if (retryDelay != null && retryDelay > 0 && retryCount != null && retryCount > 0)
        {
            return RefreshFuns.retry(
                    retryCount, 
                    retryDelay, 
                    defaultUploader
            );
        }
        
        return defaultUploader;
    }
}
