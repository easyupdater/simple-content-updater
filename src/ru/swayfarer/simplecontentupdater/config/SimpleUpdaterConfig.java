package ru.swayfarer.simplecontentupdater.config;

import java.util.concurrent.ExecutorService;

import ru.swayfarer.iocv3.annotation.ContextConfiguration;
import ru.swayfarer.iocv3.annotation.helpers.Singleton;
import ru.swayfarer.simplecontentupdater.io.RetryFileDownloader;
import ru.swayfarer.simplecontentupdater.settings.SimpleUpdaterSettings;
import ru.swayfarer.swl3.crypto.CryptoUtils;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.io.link.RLUtils;
import ru.swayfarer.swl3.io.link.ResourceLink;
import ru.swayfarer.swl3.string.converters.StringConverters;
import ru.swayfarer.swl3.swconf2.helper.SwconfIO;
import ru.swayfarer.swl3.system.SystemUtils;
import ru.swayfarer.swl3.thread.executor.queued.QueuedExecutor;
import ru.swayfarer.swl3.threads.lock.CounterLock;
import ru.swayfarer.swl3.updaterv3.context.UpdaterContext;
import ru.swayfarer.swl3.updaterv3.io.FilesRefreshFuns;
import ru.swayfarer.swl3.updaterv3.refresher.UpdateRefresher;
import ru.swayfarer.swl3.updaterv3.scanner.UpdateScanner;

@ContextConfiguration
public class SimpleUpdaterConfig {
    
    public static final String settingsLocation = "SIMPLE_UPDATER_CONFIG_LOCATION";

    @Singleton
    public UpdateScanner updateScanner(UpdaterContext updaterContext)
    {
        return new UpdateScanner(updaterContext);
    }
    
    @Singleton
    public CounterLock updatableContentRefreshCounter()
    {
        return new CounterLock();
    }
    
    @Singleton
    public SwconfIO swconfIO()
    {
        return new SwconfIO();
    }
    
    @Singleton
    public UpdateRefresher updateRefresher(UpdaterContext updaterContext, ExecutorService refreshActionExecutor, CounterLock updatableContentRefreshCounter)
    {
        return new UpdateRefresher(updaterContext)
            .setActionExecutor((e) -> {
                updatableContentRefreshCounter.counter.incrementAndGet();
                
                refreshActionExecutor.execute(() -> {
                    try
                    {
                        e.apply();
                    }
                    finally
                    {
                        updatableContentRefreshCounter.reduce();
                    }
                });
            })
        ;
    }
    
    @Singleton
    public UpdaterContext updaterContext(
            CryptoUtils cryptoUtils,
            ExceptionsHandler exceptionsHandler, 
            RLUtils rlUtils,
            RetryFileDownloader fileEntryUploader
    )
    {
        return UpdaterContext.builder()
                .cryptoUtils(cryptoUtils)
                .exceptionsHandler(exceptionsHandler)
                .rlUtils(rlUtils)
                .fileEntryRemover(FilesRefreshFuns.remove())
                .fileEntryUploader(fileEntryUploader)
                .build()
        ;
    }
    
    @Singleton
    public ExceptionsHandler exceptionsHandler()
    {
        return new ExceptionsHandler();
    }
    
    @Singleton
    public CryptoUtils cryptoUtils()
    {
        return new CryptoUtils();
    }
    
    @Singleton
    public SystemUtils systemUtils(RLUtils rlUtils)
    {
        var systemUtils = new SystemUtils();
        systemUtils.propertyConverters = new StringConverters().defaultTypes(rlUtils);
        
        return systemUtils;
    }
    
    @Singleton
    public ExecutorService refreshActionExecutor(SimpleUpdaterSettings updaterSettings)
    {
        var queuedExecutor = new QueuedExecutor();
        queuedExecutor.getSettings()
            .maxThreadsCount(updaterSettings.getMaxThreads())
            .displayName("Updater")
        ;
        
        return queuedExecutor;
    }
    
    @Singleton
    public RLUtils rlUtils()
    {
        return RLUtils.defaultUtils();
    }
    
    @Singleton
    public SimpleUpdaterSettings updaterSettings(SystemUtils systemUtils, RLUtils rlUtils)
    {
        var property = systemUtils.findEnv(settingsLocation, "f:simpleContentUpdater/config.yml");
        var ret = new SimpleUpdaterSettings();
        
        ret.config()
            .load(property.getValue(ResourceLink.class))
//            .save()
//            .enableAutoSave(500)
        ;
        
        return ret;
    }
}
