package ru.swayfarer.simplecontentupdater.tracking.postupdate;

import lombok.NonNull;
import ru.swayfarer.swl3.collections.list.ExtendedList;

public interface IUpdateEndListener {
    public String getId();
    public void listen(@NonNull ExtendedList<String> args);
}
