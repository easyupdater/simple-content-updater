package ru.swayfarer.simplecontentupdater.tracking.postupdate;

import lombok.NonNull;
import lombok.SneakyThrows;
import ru.swayfarer.iocv3.annotation.Injection;
import ru.swayfarer.iocv3.annotation.helpers.Singleton;
import ru.swayfarer.simplecontentupdater.settings.SimpleUpdaterSettings;
import ru.swayfarer.swl3.collections.list.ExtendedList;

@Singleton
public class ShellPostUpdateListener implements IUpdateEndListener{
    
    public static String id = "shell.command";
    
    @Injection
    public SimpleUpdaterSettings updaterSettings;
    
    @Override
    public String getId()
    {
        return id;
    }

    @SneakyThrows
    @Override
    public void listen(@NonNull ExtendedList<String> args)
    {
        var pb = new ProcessBuilder(args);
        pb.directory(updaterSettings.getRootDir());
        pb.redirectErrorStream();
        pb.start();
    }
}
