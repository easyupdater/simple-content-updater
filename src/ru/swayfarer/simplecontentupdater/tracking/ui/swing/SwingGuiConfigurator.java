package ru.swayfarer.simplecontentupdater.tracking.ui.swing;

import static ru.swayfarer.simplecontentupdater.tracking.ui.swing.SwingUpdateTracker.UPDATER_SUBSCRIPTION_ID;
import static ru.swayfarer.simplecontentupdater.tracking.ui.swing.SwingUpdateTracker.prepare;

import java.util.concurrent.atomic.AtomicLong;

import lombok.NonNull;
import ru.swayfarer.iocv3.annotation.Injection;
import ru.swayfarer.iocv3.annotation.helpers.Singleton;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.updaterv3.context.UpdaterContext;

@Singleton
public class SwingGuiConfigurator {
    
    @Injection
    public UpdaterContext updaterContext;
    
    public void configure(@NonNull GuiUpdateTracker gui)
    {
        subscribeToScan(gui);
        subscribeToUpdate(gui);
        
        SwingUpdateTracker.exec(() -> {
            gui.setVisible(true);
        });
    }
    
    public void subscribeToScan(@NonNull GuiUpdateTracker gui)
    {
        var scanEvents = updaterContext.getScanEvents();
        prepare(scanEvents.eventScanStart)
            .subscribe(
                (scanStartEvent) -> {
                    SwingUpdateTracker.exec(() -> {
                        var eventFileScan = scanEvents.eventFileEntryScanned;
                        
                        var totalFilesToScan = scanStartEvent.getFilesToScan().size();
                        var rootDirName = scanStartEvent.getScanContext().getRootDir().getName();
                        
                        var totalScannedFiles = new AtomicLong();
                        eventFileScan.subscribe(
                                () -> {
                                    SwingUpdateTracker.exec(() -> {
                                        var newHeaderText = "Scanning content in '" + rootDirName + "' (" + totalScannedFiles.getAndIncrement() + "/" + totalFilesToScan + ")";
                                        gui.setHeaderText(newHeaderText);
                                    });
                                }
                            )
                            .tag(SwingUpdateTracker.UPDATER_SUBSCRIPTION_ID)
                        ;
                    });
                }
            )
            .tag(UPDATER_SUBSCRIPTION_ID)
        ;
    }
    
    public void subscribeToUpdate(@NonNull GuiUpdateTracker gui)
    {
        prepare(updaterContext.getEvents().eventContentStart)
            .subscribe(
                    (updatableContent) -> {
                        SwingUpdateTracker.exec(() -> {
                            var events = updaterContext.getEvents();
                            
                            var totalFilesUpdated = new AtomicLong();
                            var totalFilesToUpdate = updatableContent.getFiles().size();
    
                            SwingUpdateTracker
                                .prepare(events.eventFileComplete)
                                .subscribe(
                                        (completedFileEntry) -> {
                                            SwingUpdateTracker.exec(() -> {
                                                gui.removeActiveFileListEntry(completedFileEntry); 
                                                
                                                var contentName = StringUtils.orDefault(updatableContent.getUpdatableContentInfo().getName(), "<unknown>");
                                                var headerText = "Updating '" + contentName + "' (" + totalFilesUpdated.incrementAndGet() + "/" + totalFilesToUpdate + ")";
                                                gui.setHeaderText(headerText);
                                                
                                                gui.updateHeight();
                                            });
                                        }
                                )
                                .tag(UPDATER_SUBSCRIPTION_ID)
                            ;
                        });
                    }
            )
            .tag(UPDATER_SUBSCRIPTION_ID)
        ;
        
        prepare(updaterContext.getEvents().eventContentEnd)
            .subscribe(
                    (e) -> {
                        SwingUpdateTracker.exec(() -> {
                            gui.setHeaderText("Complete!");
                         });
                    })
            .tag(UPDATER_SUBSCRIPTION_ID)
        ;
    }
}
