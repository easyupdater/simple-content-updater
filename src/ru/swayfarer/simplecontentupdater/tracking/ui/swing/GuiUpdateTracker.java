package ru.swayfarer.simplecontentupdater.tracking.ui.swing;

import java.awt.BorderLayout;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;

import lombok.NonNull;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.swing.box.JVBox;
import ru.swayfarer.swl3.swing.constants.ContentAlignment;
import ru.swayfarer.swl3.swing.utils.SwingUtils;
import ru.swayfarer.swl3.updaterv3.content.FileEntry;

@SuppressWarnings("serial")
public class GuiUpdateTracker extends JFrame {

    public JVBox boxFrameContent = new JVBox();
    
    public JLabel lblHeader = new JLabel();
    public JVBox boxFileEntries = new JVBox();
    
    public ExtendedMap<String, JComponent> activeFileEntries = new ExtendedMap<>().synchronize();
    
    public AtomicInteger boxEntriesCount = new AtomicInteger();
    
    public GuiUpdateTracker()
    {
        setSize(600, 100);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        lblHeader.setText("Waiting...");
        
        SwingUtils.size().fix(lblHeader, -1, 30);
        
        SwingUtils.alignment().text().set(lblHeader, ContentAlignment.HCENTER, ContentAlignment.VCENTER);
        
        boxFrameContent.addItem(lblHeader);
        boxFrameContent.addItem(new JScrollPane(boxFileEntries));
        
        getContentPane().add(boxFrameContent, BorderLayout.CENTER);
    }
    
    public GuiUpdateTracker setHeaderText(@NonNull String text)
    {
        this.lblHeader.setText(text);
        return this;
    }
    
    public JComponent addFileProgressEntry(@NonNull FileEntry fileEntry, @NonNull IObservable<Long> eventEntryUpdateProgress)
    {
        var boxContent = new JVBox();
        var lblFileName = new JLabel();
        var pbUpdateProgress = new JProgressBar();
        
        var maxEntrySizeInBytes = fileEntry.getSizeInBytes();
        
        lblFileName.setText(fileEntry.getPath());
        boxContent.setAlignment(ContentAlignment.TOP);
        
        if (maxEntrySizeInBytes > 0)
        {
            pbUpdateProgress.setMaximum(100);
            pbUpdateProgress.setValue(0);
            
            SwingUpdateTracker.prepare(eventEntryUpdateProgress);
            eventEntryUpdateProgress.subscribe(
                    () -> {
                        SwingUpdateTracker.exec(() -> {
                            pbUpdateProgress.setValue(pbUpdateProgress.getValue() + 1);
                            pbUpdateProgress.repaint();
                        });
                    }
                )
                .tag(SwingUpdateTracker.UPDATER_SUBSCRIPTION_ID)
            ;
        }
        else
        {
            pbUpdateProgress.setIndeterminate(true);
        }
        
        SwingUtils.size().fix(boxContent, -1, 50);
        
        boxContent.addItem(lblFileName, 10, 0, 10, 0);
        boxContent.addItem(pbUpdateProgress, 10, 0, 10, 0);
        
        activeFileEntries.put(fileEntry.getPath(), boxContent);
        boxFileEntries.addItem(boxContent);
        boxEntriesCount.incrementAndGet();
        
        return boxContent;
    }
    
    public void updateHeight()
    {
        var entriesCount = boxEntriesCount.get();
        
        var totalHeight = 80 + entriesCount * 50;
        
        setSize(getWidth(), Math.max(30, totalHeight));
        repaint();
    }
    
    public void removeActiveFileListEntry(@NonNull FileEntry fileEntry)
    {
        var path = fileEntry.getPath();
        var entryRecord = activeFileEntries.get(path);
        
        if (entryRecord != null)
        {
            boxEntriesCount.decrementAndGet();
            boxFileEntries.removeItem(entryRecord);
            activeFileEntries.remove(path);
        }
    }
}
