package ru.swayfarer.simplecontentupdater.tracking.ui.swing;

import javax.swing.UIManager;

import com.nilo.plaf.nimrod.NimRODLookAndFeel;
import com.nilo.plaf.nimrod.NimRODTheme;

import lombok.SneakyThrows;
import ru.swayfarer.iocv3.annotation.Injection;
import ru.swayfarer.iocv3.annotation.helpers.Singleton;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.io.link.RLUtils;

@Singleton
public class SwingLafManager {
    
    @Injection
    public RLUtils rlUtils;
    
    @Injection
    public ExceptionsHandler exceptionsHandler;
    
    @SneakyThrows
    public void initLaf()
    {
        var theme = new NimRODTheme("assets/nighlty.theme");
        final NimRODLookAndFeel laf = new NimRODLookAndFeel();
        NimRODLookAndFeel.setCurrentTheme(theme);
        
        UIManager.setLookAndFeel(laf);
    }
}
