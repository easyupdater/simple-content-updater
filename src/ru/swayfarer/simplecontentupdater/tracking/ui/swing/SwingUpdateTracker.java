package ru.swayfarer.simplecontentupdater.tracking.ui.swing;

import java.awt.EventQueue;

import javax.swing.JOptionPane;

import lombok.NonNull;
import lombok.SneakyThrows;
import ru.swayfarer.iocv3.annotation.Injection;
import ru.swayfarer.iocv3.annotation.OnInit;
import ru.swayfarer.iocv3.annotation.helpers.Singleton;
import ru.swayfarer.simplecontentupdater.tracking.IUpdateTracker;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0NoR;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.updaterv3.content.FileEntry;
import ru.swayfarer.swl3.updaterv3.context.UpdaterContext;
import ru.swayfarer.swl3.updaterv3.exception.RefreshFileEntryException;

@Singleton
public class SwingUpdateTracker implements IUpdateTracker {

    public static String UPDATER_ACTION_ID = "ui.swing";
    public static String UPDATER_SUBSCRIPTION_ID = SwingUpdateTracker.class.getCanonicalName() + ".SUB.ID";

    @Injection
    public SwingLafManager swingLafManager;
    
    @Injection
    public UpdaterContext updaterContext;
    
    @Injection
    public SwingGuiConfigurator swingGuiConfigurator;
    
    public GuiUpdateTracker gui;
    
    @Override
    public String getId()
    {
        return UPDATER_ACTION_ID;
    }

    @OnInit
    public void onBeanInit()
    {
        swingLafManager.initLaf();
        gui = new GuiUpdateTracker();
        swingGuiConfigurator.configure(gui);
    }

    @Override
    public void addFileProgressTracking(FileEntry fileEntry, IObservable<Long> progress)
    {
        SwingUpdateTracker.exec(() -> {
            gui.addFileProgressEntry(fileEntry, progress);
            gui.updateHeight();
        });
    }

    @Override
    public void onEntryError(RefreshFileEntryException ex)
    {
        JOptionPane.showMessageDialog(gui, "Error occured while updating content: \n" + ex.toString() + "\nUpdater will close after this dialog!");
        System.exit(1);
    }
    
    @SneakyThrows
    public static void exec(@NonNull IFunction0NoR fun)
    {
        if (!EventQueue.isDispatchThread())
            EventQueue.invokeAndWait(fun.asJavaRunnable());
        else
            fun.apply();
    }
    
    public static <T> IObservable<T> prepare(@NonNull IObservable<T> obs)
    {
        obs.removeByTag(UPDATER_SUBSCRIPTION_ID);
        return obs;
    }

    @Override
    public void onUpdatingStart() {}

    @Override
    public void onUpdatingEnd() {}
}
