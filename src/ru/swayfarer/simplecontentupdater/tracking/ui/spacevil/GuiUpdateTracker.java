package ru.swayfarer.simplecontentupdater.tracking.ui.spacevil;

import com.spvessel.spacevil.DialogWindow;
import com.spvessel.spacevil.Label;
import com.spvessel.spacevil.ListBox;
import com.spvessel.spacevil.ProgressBar;
import com.spvessel.spacevil.Prototype;
import com.spvessel.spacevil.TitleBar;
import com.spvessel.spacevil.VerticalStack;
import com.spvessel.spacevil.Flags.ItemAlignment;
import com.spvessel.spacevil.Flags.MSAA;
import com.spvessel.spacevil.Flags.SizePolicy;
import com.spvessel.spacevil.Flags.VisibilityPolicy;

import lombok.NonNull;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0NoR;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.observable.property.ObservableProperty;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.thread.executor.queued.QueuedExecutor;
import ru.swayfarer.swl3.updaterv3.content.FileEntry;
import ru.swayfarer.swl3.updaterv3.context.UpdaterContext;
import ru.swayfarer.swl3.updaterv3.scanner.ScanFileEvent;

public class GuiUpdateTracker extends DialogWindow{

    public ListBox listFileEntries;
    public Label lblHeader;
    public VerticalStack vsContent;
    public ProgressBar pbScan;
    
    public QueuedExecutor uiActionExecutor;

    public ExtendedMap<String, Prototype> activeFileEntries = new ExtendedMap<>().synchronize();
    public ObservableProperty<Integer> updatedFilesCount = Observables.createProperty(0);
    
    public void initWindowParams()
    {
        setParameters("Content Updater", "Content Updater", 600, 150);
        setMinSize(400, 150); // min size of the window
        setAntiAliasingQuality(MSAA.MSAA_8X); // antialiasing quality, default: msaa 4x
        setPadding(2, 2, 2, 2); // indents for items
        setPosition(200, 200); // position on the screen where the window appears
        
        isBorderHidden = true;
        isResizable = true;
        isAlwaysOnTop = true; 
        isTransparent = true;
    }
    
    public void initWindowContent()
    {
        vsContent = new VerticalStack();
        
        setBackground(UiStyles.bgColor);
        
        var titleBar = new TitleBar("Updater");

        lblHeader = new Label();
        lblHeader.setText("Stand by.");
        lblHeader.setFontSize(15);
        lblHeader.setAlignment(ItemAlignment.TOP, ItemAlignment.HCENTER);
        lblHeader.setTextAlignment(ItemAlignment.VCENTER, ItemAlignment.HCENTER);
        lblHeader.setMaxHeight(40);
        
        pbScan = new ProgressBar();
        
        listFileEntries = new ListBox();
        listFileEntries.setSelectionVisible(false);
        listFileEntries.setVScrollBarPolicy(VisibilityPolicy.AS_NEEDED);

        addItem(vsContent);
        
        vsContent.addItems(titleBar, lblHeader, pbScan, listFileEntries);
    }
    
    @Override
    public void initWindow()
    {
        initExecutor();
        
        executeInUI(() -> {
            initWindowParams();
            initWindowContent();
        });
    }
    
    public void initExecutor()
    {
       uiActionExecutor = new QueuedExecutor();
    
       uiActionExecutor.getSettings()
           .minThreadsCount(1)
           .maxThreadsCount(1)
           .keepAliveTime(100)
       ;
    }

    public void executeInUI(@NonNull IFunction0NoR fun)
    {
        uiActionExecutor.exec(fun);
    }
    
    public void createFileEntryProgress(FileEntry fileEntry, IObservable<Long> fileDowloadProgressHandler)
    {
        executeInUI(() -> {
            var lblFilePath = new Label();
    
            var path = fileEntry.getPath();
    
            lblFilePath.setText(path);
    
            lblFilePath.setTextAlignment(ItemAlignment.LEFT, ItemAlignment.VCENTER);
            lblFilePath.setTextMargin(10, 0, 0, 0);
            lblFilePath.setMinHeight(20);
            
            var pbDownloadProgress = new ProgressBar();
            
            pbDownloadProgress.setMinValue(0);
            pbDownloadProgress.setMaxValue(100);
            pbDownloadProgress.setWidthPolicy(SizePolicy.EXPAND);
            pbDownloadProgress.setCurrentValue(0);
    
            
            var maxFileSize = fileEntry.getSizeInBytes();
            
            fileDowloadProgressHandler.subscribe(
                    (value) -> {
                        float valuef = (float) value;
                        float maxSizef = (float) maxFileSize;
                        
                        var progreesBarValue = (int) (valuef / maxSizef * 100);
                        pbDownloadProgress.setCurrentValue(progreesBarValue);
                    }
                )
                .tag(UIWindowUpdateTracker.UPDATER_SUBSCRIPTION_TAG)
            ;
            
            var stackfileEntry = new VerticalStack();
            
            stackfileEntry.setMargin(10, 0, 10, 0);
            stackfileEntry.setHeight(50);
            stackfileEntry.setSizePolicy(SizePolicy.EXPAND, SizePolicy.FIXED);
            
            listFileEntries.addItem(stackfileEntry);
            stackfileEntry.addItem(lblFilePath);
            stackfileEntry.addItem(pbDownloadProgress);
            
            activeFileEntries.put(path, stackfileEntry);
        });
    }

    public void removeFileEntryProgress(FileEntry fileEntry)
    {
        executeInUI(() -> {
            var item = activeFileEntries.get(fileEntry.getPath());
            
            if (item != null)
                listFileEntries.removeItem(item);
        });
    }
    
    public GuiUpdateTracker subscribeUpdate(@NonNull UpdaterContext updaterContext)
    {
        var events = updaterContext.getEvents();
        
        events.eventContentStart.subscribe(
                (updateContent) -> {
                    var info = updateContent.getUpdatableContentInfo();
                    
                    var height = getHeight();
                    
                    if (height < 260)
                        executeInUI(() -> {
                            setHeight(260);
                        });
                    
                    if (info != null)
                    {
                        UIWindowUpdateTracker.removeTrackerSubs(events.eventFileComplete);
                        UIWindowUpdateTracker.removeTrackerSubs(events.eventFileFail);
                        
                        updatedFilesCount.setValue(0);
                        UIWindowUpdateTracker.removeTrackerSubs(updatedFilesCount.eventPostChange);
                        
                        updatedFilesCount.eventPostChange.subscribe(
                                (value) -> {
                                    lblHeader.setText("Updating '" + StringUtils.orDefault(info.getName(), "<unnamed>") + "' (" + value.getNewValue() + "/ "  + updateContent.files.size() + " )");
                                }
                            )
                            .tag(UIWindowUpdateTracker.UPDATER_SUBSCRIPTION_TAG)
                        ;
                        
                        events.eventFileComplete.subscribe(
                                (fileEntry) -> {
                                    removeFileEntryProgress(fileEntry);
                                    updatedFilesCount.setValue(updatedFilesCount.getValue() + 1);
                                }
                            )
                            .tag(UIWindowUpdateTracker.UPDATER_SUBSCRIPTION_TAG)
                        ;
                    }
                }
            )
            .tag(UIWindowUpdateTracker.UPDATER_SUBSCRIPTION_TAG)
        ;
        
        return this;
    }

    public void subscribeScanProgress(int filesTotalCount, IObservable<ScanFileEvent> eventFileScan)
    {
        lblHeader.setText("Scanning content...");
        pbScan.setMaxValue(filesTotalCount);
        pbScan.setCurrentValue(0);
        pbScan.setValueVisible(true);
        
        UIWindowUpdateTracker.removeTrackerSubs(eventFileScan);
        
        eventFileScan.subscribe(
                () -> {
                    executeInUI(() -> {
                        pbScan.setCurrentValue(pbScan.getCurrentValue() + 1);
                    });
                }
            )
            .tag(UIWindowUpdateTracker.UPDATER_SUBSCRIPTION_TAG)
        ;
    }
    
    public void hideScanProgress()
    {
        pbScan.setHeight(0);
    }

    public void showScanProgress()
    {
        pbScan.setHeight(30);
    }

    @Override
    public void close()
    {
        uiActionExecutor.shutdown();
        super.close();
    }
}
