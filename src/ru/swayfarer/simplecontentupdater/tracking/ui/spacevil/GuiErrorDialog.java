package ru.swayfarer.simplecontentupdater.tracking.ui.spacevil;

import java.awt.Color;

import com.spvessel.spacevil.DialogWindow;
import com.spvessel.spacevil.Label;
import com.spvessel.spacevil.TextArea;
import com.spvessel.spacevil.TitleBar;
import com.spvessel.spacevil.VerticalStack;

import lombok.NonNull;

public class GuiErrorDialog extends DialogWindow {

    public VerticalStack contentStack;
    public Label lblHeader;
    public Label lblText;
    public TitleBar titleBar;
    
    public TextArea txtArea;
    
    public void initWindow(@NonNull String errrorStr)
    {
        setParameters("Error", "Error", 600, 150);
        setBackground(UiStyles.bgColor);
        
        contentStack = new VerticalStack();
        
        titleBar = new TitleBar("Error!");
        
        lblHeader = new Label();
        lblHeader.setText("Error while updating! See log file!");
        lblHeader.setBackground(192, 83, 83);
        lblHeader.setForeground(Color.white);
        lblHeader.setMinHeight(40);
        lblHeader.setMaxHeight(40);
        lblHeader.setFontSize(17);
        lblHeader.setTextMargin(15, 0, 0, 0);
        lblHeader.setMargin(5, 5, 5, 5);
        
        lblHeader.setBorderRadius(10);
        
        lblText = new Label();
        
        txtArea = new TextArea();
        txtArea.setEditable(false);
        txtArea.setText(errrorStr);
        txtArea.setFontSize(14);
        txtArea.setMargin(5, 10, 5, 5);
        
        addItem(contentStack);
        
        contentStack.addItem(titleBar);
        contentStack.addItem(lblHeader);
        contentStack.addItem(txtArea);
        
        isBorderHidden = true;
        
        eventClose.add(() -> {
            System.exit(1);
        });
    }

    @Override
    public void initWindow()
    {
        
    }
}
