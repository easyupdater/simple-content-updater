package ru.swayfarer.simplecontentupdater.tracking.ui.spacevil;

import java.awt.Color;

import com.spvessel.spacevil.ActiveWindow;
import com.spvessel.spacevil.Label;
import com.spvessel.spacevil.ListBox;
import com.spvessel.spacevil.ProgressBar;
import com.spvessel.spacevil.TextArea;
import com.spvessel.spacevil.TextEdit;
import com.spvessel.spacevil.TitleBar;
import com.spvessel.spacevil.WrapGrid;
import com.spvessel.spacevil.Common.DefaultsService;
import com.spvessel.spacevil.Decorations.Style;
import com.spvessel.spacevil.Decorations.ThemeStyle;

public class UiStyles {

    public static Color clearColor = new Color(0, 0, 0, 0);
    public static Color bgColor = new Color(24, 26, 37, 255);
    public static Color fgColor = new Color(118, 126, 139);
    public static Color progressBarFillingColor = new Color(51, 133, 113);
    public static Color progressBarFgColor = new Color(153, 194, 184);
    public static Color progressBarBgColor = new Color(22, 33, 45);

    public static void initElegreTheme()
    {
        DefaultsService.setDefaultTheme(getElegreTheme());
    }
    
    public static ThemeStyle getElegreTheme()
    {
        var defaultTheme = DefaultsService.getDefaultTheme();

        defaultTheme.replaceDefaultItemStyle(ListBox.class, getListBoxStyle());
        defaultTheme.replaceDefaultItemStyle(Label.class, getLabelStyle());
        defaultTheme.replaceDefaultItemStyle(WrapGrid.class, getWrapGridStyle());
        defaultTheme.replaceDefaultItemStyle(ProgressBar.class, getProgressBarStyle());
        defaultTheme.replaceDefaultItemStyle(TitleBar.class, getWindowTitleStyle());
        defaultTheme.replaceDefaultItemStyle(TextArea.class, getTextAreaStyle());
        defaultTheme.replaceDefaultItemStyle(TextEdit.class, getTextEditStyle());
        
        return defaultTheme;
    }
    
    public static Style getCoreWindowStyle()
    {
        var style = DefaultsService.getDefaultStyle(ActiveWindow.class);
        setForeground(fgColor, style);
        setBackground(bgColor, style);
        return style;
    }
    
    public static Style getWindowTitleStyle()
    {
        var style = DefaultsService.getDefaultStyle(TitleBar.class);
        setForeground(fgColor, style);
        setBackground(bgColor, style);
        return style;
    }
    
    public static Style getListBoxStyle()
    {
        var style = DefaultsService.getDefaultStyle(ListBox.class);
        setForeground(fgColor, style);
        setBackground(bgColor, style);
        return style;
    }
    
    public static Style getLabelStyle()
    {
        var style = DefaultsService.getDefaultStyle(Label.class);
        setForeground(fgColor, style);
        setBackground(clearColor, style);
        return style;
    }
    
    public static Style getWrapGridStyle()
    {
        var style = DefaultsService.getDefaultStyle(WrapGrid.class);
        setForeground(fgColor, style);
        setBackground(bgColor, style);
        return style;
    }
    
    public static Style getProgressBarStyle()
    {
        var style = DefaultsService.getDefaultStyle(ProgressBar.class);
        setForeground(Color.WHITE, style);
        setBackground(progressBarBgColor, style);
        
        var progressFillStyle = style.getInnerStyle("progressbar");
        setBackground(progressBarFillingColor, progressFillStyle);
        return style;
    }
    
    public static Style getTextEditStyle()
    {
        var style = DefaultsService.getDefaultStyle(TextEdit.class);
        setForeground(Color.WHITE, style);
        setBackground(progressBarBgColor, style);
        
        return style;
    }
    
    public static Style getTextAreaStyle()
    {
        var style = DefaultsService.getDefaultStyle(TextArea.class);
        setForeground(Color.WHITE, style);
        setBackground(progressBarBgColor, style);
        
        var editStyle = style.getInnerStyle("textedit");

        setForeground(new Color(146, 146, 146), editStyle);
        setBackground(progressBarBgColor, editStyle);
        
        return style;
    }
    
    public static void setForeground(Color color, Style style)
    {
        style.setForeground(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }
    
    public static void setBackground(Color color, Style style)
    {
        style.setBackground(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    }
}
