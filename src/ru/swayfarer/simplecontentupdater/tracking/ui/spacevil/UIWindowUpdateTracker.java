package ru.swayfarer.simplecontentupdater.tracking.ui.spacevil;

import com.spvessel.spacevil.Common.CommonService;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.annotation.Injection;
import ru.swayfarer.iocv3.annotation.helpers.Singleton;
import ru.swayfarer.simplecontentupdater.tracking.IUpdateTracker;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.thread.ThreadsUtils;
import ru.swayfarer.swl3.updaterv3.content.FileEntry;
import ru.swayfarer.swl3.updaterv3.context.UpdaterContext;
import ru.swayfarer.swl3.updaterv3.exception.RefreshFileEntryException;

@Singleton
@Data
@Accessors(chain = true)
public class UIWindowUpdateTracker implements IUpdateTracker {

    public static String UPDATER_SUBSCRIPTION_TAG = "#" + UIWindowUpdateTracker.class.getCanonicalName();
    
    public static String id = "ui.window";
    
    @Injection
    public UpdaterContext updaterContext;
    
    public GuiUpdateTracker guiUpdateTracker;
    
    @Override
    public String getId()
    {
        return id;
    }

    @Override
    public void onUpdatingStart()
    {
        CommonService.initSpaceVILComponents();
        UiStyles.initElegreTheme();
        guiUpdateTracker = new GuiUpdateTracker();
        guiUpdateTracker.subscribeUpdate(updaterContext);
        
        var scanEvents = updaterContext.getScanEvents();
        
        scanEvents.eventScanStart.subscribe(
                (evt) -> {
                    guiUpdateTracker.showScanProgress();
                    guiUpdateTracker.subscribeScanProgress(
                            evt.getFilesToScan().size(), 
                            scanEvents.eventFileEntryScanned
                    );
                }
            )
            .tag(UPDATER_SUBSCRIPTION_TAG)
        ;
        
        scanEvents.eventScanEnd.subscribe(
                () -> {
                    guiUpdateTracker.hideScanProgress();
                }
            )
            .tag(UPDATER_SUBSCRIPTION_TAG)
        ;
        
        var thread = new Thread(() -> {
            guiUpdateTracker.show();
        }, "SpaceVillTracker-Thread");
        
        thread.start();
    }
    
    @Override
    public void onUpdatingEnd()
    {
        ThreadsUtils.sleep(1500);
        guiUpdateTracker.close();
    }

    @Override
    public void addFileProgressTracking(FileEntry fileEntry, IObservable<Long> progress)
    {
        guiUpdateTracker.createFileEntryProgress(fileEntry, progress);
    }

    @Override
    public void onEntryError(RefreshFileEntryException ex)
    {
        var thread = new Thread(() -> {
            var errorWindow = new GuiErrorDialog();
            errorWindow.initWindow(ex.getCause().toString());
            errorWindow.show(); 
        }, "ErrorDialogThread");
        
        thread.start();
    }
    
    public static void removeTrackerSubs(@NonNull IObservable<?> obs)
    {
        obs.removeByTag(UPDATER_SUBSCRIPTION_TAG);
    }
}
