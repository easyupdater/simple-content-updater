package ru.swayfarer.simplecontentupdater.tracking;

import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.updaterv3.content.FileEntry;
import ru.swayfarer.swl3.updaterv3.exception.RefreshFileEntryException;

public interface IUpdateTracker {
    public String getId();
    
    public void onUpdatingStart();
    public void onUpdatingEnd();
    
    public void addFileProgressTracking(FileEntry fileEntry, IObservable<Long> progress);
    public void onEntryError(RefreshFileEntryException ex);
    
    public IUpdateTracker EMPTY = new IUpdateTracker() {
        
        @Override
        public void onUpdatingStart() { }
        
        @Override
        public void addFileProgressTracking(FileEntry fileEntry, IObservable<Long> progress) { }
        
        @Override
        public void onUpdatingEnd() { }
        
        @Override
        public String getId()
        {
            return "<empty>";
        }

        @Override
        public void onEntryError(RefreshFileEntryException ex)
        {
            throw ex;
        }
    };
}
