package ru.swayfarer.simplecontentupdater.settings;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.list.PriorityList;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.observable.property.ObservableProperty;
import ru.swayfarer.swl3.swconf2.config.Swconf2Config;
import ru.swayfarer.swl3.swconf2.mapper.annotations.CommentedSwconf;

@Data @EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class SimpleUpdaterSettings extends Swconf2Config{

    @CommentedSwconf
    public ListenerEntry updateListeners;
    
    @CommentedSwconf
    public ListenerEntry postUpdateActions;
    
    @CommentedSwconf
    public int maxThreads = 4;
    
    @CommentedSwconf
    public DownloadRetry retry;
    
    @CommentedSwconf
    public PriorityList<String> sources = new PriorityList<>();
    
    @CommentedSwconf
    public FileSWL rootDir = FileSWL.of("contentToUpdate/");
    
    @CommentedSwconf
    public String hashingType = "MD5";
    
    public static class ListenerEntry {
        @CommentedSwconf
        public Boolean isEnabled = false;
        
        @CommentedSwconf
        public PriorityList<Entry> actions = new PriorityList<>();
        
        @Data
        @Accessors(chain = true)
        public static class Entry {
            public String id;
            public ExtendedList<String> params;
        }
        
        public boolean isEnabled()
        {
            return Boolean.TRUE.equals(isEnabled);
        }
    }
    
    public static class DownloadRetry {
        
        @CommentedSwconf
        public ObservableProperty<Long> count = Observables.createProperty(10l);
        
        @CommentedSwconf
        public ObservableProperty<String> delay = Observables.createProperty("1s");
    }
}
