package ru.swayfarer.simplecontentupdater.downloading;

import java.util.NoSuchElementException;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.iocv3.annotation.Injection;
import ru.swayfarer.iocv3.annotation.LazyBean;
import ru.swayfarer.iocv3.annotation.OnApplicationStart;
import ru.swayfarer.iocv3.annotation.helpers.Singleton;
import ru.swayfarer.iocv3.bean.BeansList;
import ru.swayfarer.simplecontentupdater.exceptions.PostUpdateActionException;
import ru.swayfarer.simplecontentupdater.io.RetryFileDownloader;
import ru.swayfarer.simplecontentupdater.settings.SimpleUpdaterSettings;
import ru.swayfarer.simplecontentupdater.tracking.IUpdateTracker;
import ru.swayfarer.simplecontentupdater.tracking.postupdate.IUpdateEndListener;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.io.link.RLUtils;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.swconf2.helper.SwconfIO;
import ru.swayfarer.swl3.threads.lock.CounterLock;
import ru.swayfarer.swl3.updaterv3.content.UpdatableContent;
import ru.swayfarer.swl3.updaterv3.context.UpdaterContext;
import ru.swayfarer.swl3.updaterv3.exception.RefreshFileEntryException;
import ru.swayfarer.swl3.updaterv3.io.FilesRefreshFuns;
import ru.swayfarer.swl3.updaterv3.refresher.RefreshEvent;
import ru.swayfarer.swl3.updaterv3.refresher.UpdateRefresher;
import ru.swayfarer.swl3.updaterv3.scanner.UpdateScanContext;
import ru.swayfarer.swl3.updaterv3.scanner.UpdateScanner;

@LazyBean(false)
@Singleton
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class UpdateApplierService {
    
    @Injection
    public ExceptionsHandler exceptionsHandler;
    
    @Injection
    public UpdateRefresher updateRefresher;
    
    @Injection
    public SimpleUpdaterSettings updaterSettings;
    
    @Injection
    public UpdaterContext updaterContext;
    
    @Injection
    public UpdateScanner updateScanner;
    
    @Injection
    public SwconfIO swconfIO;
    
    @Injection
    public RLUtils rlUtils;
    
    @Injection
    public BeansList<IUpdateTracker> registeredUpdateListeners;
    
    @Injection
    public BeansList<IUpdateEndListener> updateEndListeners;
    
    @Injection
    public RetryFileDownloader fileEntryUploader;
    
    @Injection
    public CounterLock updatableContentRefreshCounter;
    
    public IUpdateTracker tracker = IUpdateTracker.EMPTY;
    
    @OnApplicationStart
    public void updateStart(String... args)
    {
        var contextExHandler = ExceptionsHandler.getContextHandler();
        
        contextExHandler.configure()
            .rule()
                .type(RefreshFileEntryException.class)
                .log(LogFactory.getLogger())
            .rule()
                .type(RefreshFileEntryException.class)
                .thanSkip()
                .handle((exEvt) -> {
                    var ex = exEvt.getException();
                    tracker.onEntryError((RefreshFileEntryException) ex);
                })
                
        ;
        
        var sources = updaterSettings.getSources();
        sources.updateSort();
        
        configureTracker();
        tracker.onUpdatingStart();
        
        for (var source : sources)
        {
            updateFromSource(source);
            updatableContentRefreshCounter.waitFor();
        }
        
        tracker.onUpdatingEnd();
        
        postUpdate();
    }

    public void configureTracker()
    {
        if (!CollectionsSWL.isNullOrEmpty(registeredUpdateListeners))
        {
            var updateListeners = updaterSettings.getUpdateListeners();
            
            if (updateListeners != null)
            {
                var listeners = updateListeners.actions;
                
                for (var action : listeners)
                {
                    try
                    {
                        var id = action.getId();
                        
                        if (!StringUtils.isBlank(id))
                        {
                            tracker = registeredUpdateListeners.exStream().findFirst((e) -> e.getId().equals(id)).orNull();
                        }
                    }
                    catch (Throwable e)
                    {
                        exceptionsHandler.handle(new PostUpdateActionException(e));
                    }
                }
                
                fileEntryUploader.eventUploadingStart.getSubscriptions().clear();
                
                fileEntryUploader.eventUploadingStart.subscribe((e) -> {
                    var fileEntry = e.getFileEntry();
                    var progress = e.getEventDownloadProgress();
                    tracker.addFileProgressTracking(fileEntry, progress);
                });
            }
        }
    }
    
    public void postUpdate()
    {
        if (!CollectionsSWL.isNullOrEmpty(updateEndListeners))
        {
            var postUpdateActions = updaterSettings.getPostUpdateActions();
            
            if (postUpdateActions != null && postUpdateActions.isEnabled())
            {
                var actions = postUpdateActions.actions;
                
                for (var action : actions)
                {
                    try
                    {
                        var id = action.getId();
                        
                        if (!StringUtils.isBlank(id))
                        {
                            var listener = updateEndListeners.exStream().findFirst((e) -> e.getId().equals(id));
                            
                            ExceptionsUtils.IfNot(listener.isPresent(), NoSuchElementException.class, "Can't find listener with id '" + id + "'");
                            
                            listener.get().listen(action.params);
                        }
                    }
                    catch (Throwable e)
                    {
                        exceptionsHandler.handle(new PostUpdateActionException(e));
                    }
                }
            }
        }
    }
    
    public void updateFromSource(@NonNull String source)
    {
        var localRootDir = updaterSettings.getRootDir();
        var rlink = rlUtils.createLink(source);
        
        var sourceContent = swconfIO.deserialize(UpdatableContent.class, rlink);
        
        if (sourceContent != null)
        {
            System.out.println(sourceContent.files.size());
            System.out.println(sourceContent.getUpdatableContentInfo().getFiltering());
            sourceContent.filter();
            System.out.println(sourceContent.files.size());
            
            var scanContext = UpdateScanContext.builder()
                    .contentInfo(sourceContent.getUpdatableContentInfo())
                    .rootDir(localRootDir)
                    .build()
            ;
            
            var localContent = updateScanner.scan(scanContext);
            
            var refreshEvent = RefreshEvent.builder()
                    .modelContent(sourceContent)
                    .targetContent(localContent)
                    .build()
            ;
            
            FilesRefreshFuns.setRootDir(refreshEvent, localRootDir);
            
            updateRefresher.refreshTargetAll(refreshEvent);
        }
    }
}
