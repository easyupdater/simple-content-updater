package ru.swayfarer.simplecontentupdater;

import ru.swayfarer.iocv3.annotation.IocV3Starter;
import ru.swayfarer.iocv3.helper.ContextHelper;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;

@IocV3Starter
public class SimpleContentUpdater {
    
    public static void main(String[] args)
    {
        var contextExceptionsHandler = ExceptionsHandler.logByDefault();
        ContextHelper.configureExceptionsHandler(contextExceptionsHandler);
        
        ContextHelper.startApp(args);
    }
}
