package ru.swayfarer.simplecontentupdater.exceptions;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.simplecontentupdater.settings.SimpleUpdaterSettings;

@Data @EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@SuppressWarnings("serial")
public class PostUpdateActionException extends RuntimeException{

    @NonNull
    public SimpleUpdaterSettings.ListenerEntry.Entry action;
    
    public PostUpdateActionException()
    {
        super();
    }

    public PostUpdateActionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public PostUpdateActionException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public PostUpdateActionException(String message)
    {
        super(message);
    }

    public PostUpdateActionException(Throwable cause)
    {
        super(cause);
    }

}
